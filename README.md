# README #

### What is this repository for? ###

* Creates a Dll Class library to make a get and post request from DepEd LIS website to integrate it in our system.
* Created initially for Pampanga High School to be integrated with their PRO-MIS Application
* Version 1.0.0

### How do I get set up? ###

* Edit the file credential.sample.config and specify your LIS Username and Password

### Who do I talk to? ###

* Repo owner or admin
* email me at haroldcris.abarquez@gmail.com