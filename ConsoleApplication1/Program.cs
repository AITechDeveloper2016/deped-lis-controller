﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Xml;
using LISController;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApplication1
{
	static class Program
	{
        static void Main(string[] args)
        {

            DoAction();
        }

        static void DoAction()
        {
            Sf5Uploader SF5 = new Sf5Uploader();

            var recordService = new LISController.ProMis.RecordService();
            recordService.LoadStudents();

            Console.WriteLine("Hello");

            using (System.IO.StreamWriter file = new System.IO.StreamWriter("D:\\LISLog.txt", true))
            {
                ICollection<Task> taskList = new List<Task>();

                foreach (var item in recordService.Items)
                {
                    var log = $"{item.Section} - {item.LRNId} - {item.StudentNumber} => {item.Fullname} -> {item.GeneralAverage}";

                    Console.WriteLine(DateTime.Now.ToString() + ": " + log);
                    file.WriteLine(DateTime.Now.ToString() + ": " + log);

                    //Task<bool> t = Task.Factory.StartNew(() => {
                    //    bool ret = SF5.UpdateStatus(item);
                    //    if (ret == false) WriteError(log);
                    //    return ret;
                    //});
                    //Console.WriteLine("Adding Task ");
                    	if (!SF5.UpdateStatus(item)) WriteError(log);
                    //taskList.Add(t);
                }

                //while (taskList.Count != 0)
                //{
                //    var task = Task.WhenAny(taskList);
                //    taskList.Remove(task);
                //}

                file.Close();
            }

            Console.WriteLine("Done.....");
            Console.ReadKey();
        }

        static void WriteError(string log)
		{
			using (System.IO.StreamWriter file = new System.IO.StreamWriter("D:\\LIS_ErrorLog.txt", true))
			{
				file.WriteLineAsync(log);
				file.Close();
			}
		}
	}
}