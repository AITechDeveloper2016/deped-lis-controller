﻿using System;
using System.Collections.Specialized;
using System.Security.Authentication;
using System.Text;

namespace LISController
{
    public abstract class Base
    {
	    protected readonly Library.Credential _credential = new Library.Credential();
		internal readonly MyWebClient _WebClient = new MyWebClient();

        protected void Init()
        {
	        var homeUrl = _credential.ServerUrl;
	        var userName = _credential.Username;
	        var password = _credential.Password;

            Console.WriteLine($"using {homeUrl} - {userName} : {password}");
            using (MyWebClient client = new MyWebClient())
            {
                System.Console.WriteLine("Downloading Log On Screen");
                var html = Library.Retry.Do(()=> client.DownloadString(homeUrl),TimeSpan.FromSeconds(1),2);
                System.Console.WriteLine("Done");

                //Return if Logged In
                if (html.Contains("Sign out")) return;

                var data = new NameValueCollection
                {
                    {"_username", userName},
                    {"_password", password}
                };

                Console.WriteLine("Logging in LIS...");
                var webResponse = client.UploadValues(homeUrl + "/uis/login_check", data);
                _WebClient.CookieContainer = client.CookieContainer;

                html = Encoding.Default.GetString(webResponse);

                if (html.Contains("Please sign in")) throw new AuthenticationException("Invalid Username or Password");

            }
        }

        public Base()
        {
            Init();
        }
    }
}
