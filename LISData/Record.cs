﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LISController.Common;

namespace LISController
{
    /// <summary>
    /// Base Class
    /// </summary>
    public abstract class Record
    {
        public long StudentNumber { get; set; }
        public string LRNId { get; set; }        
        public decimal GeneralAverage { get; set; }

        public string Fullname { get; set; }

        public string Section { get; set; }

        internal RecordStatus RecordStatus { get; private set; }

        public bool IsHonorStudent { get; set; }

        protected Record(RecordStatus status )
        {
            RecordStatus = status;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class PromotedRecord : Record
    {
        public bool IsHonor { get; set; }

        public PromotedRecord():base(RecordStatus.Promoted)
        {
            //return;
        }   
    }


    /// <summary>
    /// 
    /// </summary>
    public class ConditionalRecord : Record
    {
        public ConditionalRecord() : base(RecordStatus.Conditional)
        {
            return;
        }
    }


    /// <summary>
    /// 
    /// </summary>
    public class RetainedRecord : Record
    {
        public RetainedRecord() : base(RecordStatus.Retained)
        {
            return;
        }
    }


    /// <summary>
    /// 
    /// </summary>
    public class DroppedRecord : Record
    {
        public DateTime AsOfDate { get; set; }
        public string ReasonCode { get; set; }
        
        public DroppedRecord() : base(RecordStatus.Dropped)
        {
            return;
        }
    }

    /***************************************
    670 > Had to take care of siblings
    675 > Early marriage/pregnancy
    680 > Parents’ attitude toward schooling
    685 > Family problems/feuds
    700 > Illness 
    705 > Overage
    710 > Death 
    715 > Drug Abuse
    720 > Poor academic performance
    725 > Lack of interest/Distractions
    730 > Hunger / Malnutrition 
    760 > Teacher factor
    765 > Physical conditions of classroom
    770 > Peer Influences
    790 > Distance between home and school
    795 > Armed conflict
    800 > Calamities / Disasters 
    805 > Child labor, work
    655 > Undetermined
    ****************************************/
}
