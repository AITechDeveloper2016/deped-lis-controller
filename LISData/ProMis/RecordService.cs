﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

using LISController.Library;

namespace LISController.ProMis
{
    public class RecordService
    {
        public IEnumerable<Record> Items { get; private set; }

        public void LoadStudents()
        {
			Credential credential = new Credential("ProMisCredential");

            var builder = new SqlConnectionStringBuilder
            {
                DataSource = credential.ServerUrl,
                InitialCatalog = "promis",
                UserID = credential.Username,
                Password = credential.Password
            };

            ICollection<LISController.Record> listOfRecords = new List<LISController.Record>();
            using (var db = new SqlConnection(builder.ToString()))
            {
                db.Open();
                string sql = @"select s.studnum
                                , dbo.fullname(Lastname,firstname,middlename,mi,0, nameextension) fullname
	                            , l.lrn 
	                            , l.lrnid LRNId
	                            , [Year]
	                            , section
	                            , promret
	                            , genaverage
	                            , dropcategory
	                            , dropdate
	                            , dropreason
                                , isnull(HonorStudent,0) HonorStudent
	                            from students s inner join students_history h on s.studnum = h.studnum
                                    inner join (select * from students_sections where batch ='2015-2016') sec on h.section = sec.sectionname and h.Year = sec.YearLevel
		                            inner join students_lis l on s.lrn = l.lrn 
                                    left join Students_TopNotcher t on  h.batch = t.Batch and h.batchtype = t.BatchType and h.studnum = t.studnum 

		                            where h.batch ='2015-2016' and h.batchtype ='regular' and school = 'pampanga high school'
		                            and year = 1
			                        --and dropcategory = ''
                                        --and section = 'electron'
		                            -- and section not in (
                                        --     'aaron', 'Abraham','Ananias','Azer','B. Lucero', 
                                        --     'bethlehem','caleb','elijah','proton')
		            
                                order by yearlevel, sectionorder desc , gender desc, lastname, firstname, middlename
                            ";

                var cmd = new SqlCommand(sql, db);
                SqlDataReader reader = cmd.ExecuteReader();

                        
                while (reader.Read())
                {

                    PromotedRecord pItem = new PromotedRecord();
                    ConditionalRecord cItem = new ConditionalRecord();
                    RetainedRecord rItem = new RetainedRecord();
                    DroppedRecord dItem = new DroppedRecord();

                    switch (reader.GetString(reader.GetOrdinal("promret")).ToLower())
                    {
                        case "prom.":
                            pItem.StudentNumber = Convert.ToInt64(reader["StudNum"]);
                            pItem.LRNId = Convert.ToString(reader["lrnId"]);
                            pItem.Fullname = Convert.ToString(reader["Fullname"]);
                            pItem.Section = reader.GetString(reader.GetOrdinal("Section"));
                            pItem.GeneralAverage = reader.GetDecimal(reader.GetOrdinal("GenAverage"));
                            pItem.IsHonor = reader.GetBoolean(reader.GetOrdinal("HonorStudent"));

                            listOfRecords.Add(pItem);
                            break;

                        case "ireg.":
                            cItem.StudentNumber = Convert.ToInt64(reader["StudNum"]);
                            cItem.LRNId = Convert.ToString(reader["lrnId"]);
                            cItem.Fullname = Convert.ToString(reader["Fullname"]);
                            cItem.Section = reader.GetString(reader.GetOrdinal("Section"));
                            cItem.GeneralAverage = reader.GetDecimal(reader.GetOrdinal("GenAverage"));

                            listOfRecords.Add(cItem);
                            break;


                        case "ret.":
                            rItem.StudentNumber = Convert.ToInt64(reader["StudNum"]);
                            rItem.LRNId = Convert.ToString(reader["lrnId"]);
                            rItem.Fullname = Convert.ToString(reader["Fullname"]);
                            rItem.Section = reader.GetString(reader.GetOrdinal("Section"));
                            rItem.GeneralAverage = reader.GetDecimal(reader.GetOrdinal("GenAverage"));

                            listOfRecords.Add(rItem);
                            break;


                        case "":


                            switch (reader.GetString(reader.GetOrdinal("DropCategory")).ToLower())
                            {
                                case "dropped":
                                        dItem.StudentNumber = Convert.ToInt64(reader["StudNum"]);
                                        dItem.LRNId = Convert.ToString(reader["lrnId"]);
                                        dItem.Fullname = Convert.ToString(reader["Fullname"]);
                                        dItem.Section = reader.GetString(reader.GetOrdinal("Section"));
                                        dItem.GeneralAverage = reader.GetDecimal(reader.GetOrdinal("GenAverage"));

                                        dItem.AsOfDate = Convert.ToDateTime(reader["DropDate"]);

                                        /***************************************
                                        670 > Had to take care of siblings
                                        675 > Early marriage/pregnancy
                                        680 > Parents’ attitude toward schooling
                                        685 > Family problems/feuds
                                        700 > Illness 
                                        705 > Overage
                                        710 > Death 
                                        715 > Drug Abuse
                                        720 > Poor academic performance
                                        725 > Lack of interest/Distractions
                                        730 > Hunger / Malnutrition 
                                        760 > Teacher factor
                                        765 > Physical conditions of classroom
                                        770 > Peer Influences
                                        790 > Distance between home and school
                                        795 > Armed conflict
                                        800 > Calamities / Disasters 
                                        805 > Child labor, work
                                        655 > Undetermined
                                        ****************************************/
                                        switch (Convert.ToString(reader["DropReason"]).ToLower())
                                        {
                                        case "change of residence": dItem.ReasonCode = "790"; break;

                                        case "deceased": dItem.ReasonCode = "710"; break;

                                        case "early marriage": dItem.ReasonCode = "675"; break;

                                        case "employment": dItem.ReasonCode = "805"; break;

                                        case "family problem": dItem.ReasonCode = "685"; break;

                                        case "financial difficulties": dItem.ReasonCode = "680"; break;

                                        case "health problem": dItem.ReasonCode = "700"; break;

                                        case "lack of credentials": dItem.ReasonCode = "680"; break;

                                        case "lack of interest": dItem.ReasonCode = "725"; break;

                                        case "misbehavior": dItem.ReasonCode = "770"; break;

                                        case "personal problem": dItem.ReasonCode = "685"; break;

                                        case "truancy": dItem.ReasonCode = "725"; break;

                                        case "went abroad": dItem.ReasonCode = "790"; break;
                                        }

                                        listOfRecords.Add(dItem);
                                        break;


                                case "transferred":
                                        dItem.StudentNumber = Convert.ToInt64(reader["StudNum"]);
                                        dItem.LRNId = Convert.ToString(reader["lrnId"]);
                                        dItem.Fullname = Convert.ToString(reader["Fullname"]);
                                        dItem.Section = reader.GetString(reader.GetOrdinal("Section"));
                                        dItem.GeneralAverage = reader.GetDecimal(reader.GetOrdinal("GenAverage"));

                                        dItem.AsOfDate = Convert.ToDateTime(reader["DropDate"]);
                                        dItem.ReasonCode = "655";

                                        listOfRecords.Add(dItem);
                                        break;
                            }

                            break;
                    }
                    
                }

                Items = listOfRecords;
            }

        }



    }
}
