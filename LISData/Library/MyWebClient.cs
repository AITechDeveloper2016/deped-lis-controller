﻿using System;
using System.Net;

namespace LISController
{
    internal class MyWebClient : WebClient
    {
        public CookieContainer CookieContainer { get; set; }

        public MyWebClient()
        {
            CookieContainer = new CookieContainer();
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest request = base.GetWebRequest(address);
            if (request is HttpWebRequest)
            {
                (request as HttpWebRequest).CookieContainer = CookieContainer;
            }
            return request;
        }
    }
}
