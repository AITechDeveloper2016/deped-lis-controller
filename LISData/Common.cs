﻿namespace LISController.Common
{
    public enum RecordStatus
    {
        Dropped,
        Transferred,
        Promoted,
        Retained,
        Conditional,
        ClearAll
    }

}